// const express = require('express')
import express from "express"
import bodyParser from "body-parser";
const app = express()

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.append('Access-Control-Allow-Origin', ['*']);
    res.append('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.append('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.post("/user", function (req, res) {
    console.log("Im here");
    const body = req.body
    return res.status(200).json(body)
})

app.get("/", (req, res) => {
    return res.send("<h1>Hello from home page 2</h1>")
})

app.get("/about", (req, res) => {
    return res.send("<h1>Hello from about</h1>")
})

app.listen(8080, () => {
    console.log("Express is listening on port 8080 ")
})