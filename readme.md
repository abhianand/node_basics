# Common js

```
    const express = require('express')
    module.exports = { f1 }
    exports = { f1 }
```

# Module JS

Inside package.json add

```
  "type": "module",
```

```
    import express from "express"
```

# Nodemon

To auto restart node js

```
    npm install --save-dev nodemon
```
Since we are not installing globally we should run the app by adding script in the package.json file

```
"scripts": {
    "listen": "nodemon index.js"
  }
```


# Restful API
Restful => GET, POST, PUT, PATCH, DELETE

GET => to get a data

POST => to create a entry/data in server/database

DELETE => to delete an entry

PUT => update an existing data {name: Pra, location: palakkad} => {name: Rem, location: Idukki}
PATCH => update an exsting data partially {name: Pra, location: palakkad} => {name: Pra, location: Idukki}
